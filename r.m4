dnl Static m4 snippets for R
dnl
dnl Shebang
dnl
define(`shebang',
``#!/usr/bin/Rscript'')dnl
dnl
dnl Entry point
dnl
define(`entrypoint',
``if (sys.nframe() == 0) {
	main()
}''dnl
)dnl
dnl
dnl Function
dnl
define(`fun',
``$1 <- function($2) {
	$3
}''dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
`shebang()`

# library(magrittr)

'fun(main,,$1)`

'entrypoint()'dnl
)dnl
dnl
dnl Exit program
dnl
define(`exit',
``quit(save=no, $1)''dnl
)dnl
dnl
dnl For
dnl
define(`for',
``for (_ in $1){

}''dnl
)dnl
