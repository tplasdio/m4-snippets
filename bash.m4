dnl Static m4 snippets for Bash shell
dnl
include(`sh.m4')dnl
dnl
define(«shebang»,
««#!/usr/bin/bash»»)dnl
dnl
define(«entrypoint»,
««if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
	main "$»«@" || exit $?
fi»»dnl
)dnl
dnl
dnl Function
dnl
define(«fun»,
««function $1 {
	$2
}»»dnl
)dnl
dnl
dnl Safe printing
dnl
define(«echo»,
««builtin printf -- "%s\n" $1»»dnl
)dnl
dnl
dnl Read stdin line into a variable, optionally with text before
dnl
define(«stdin»,
««read -p $2 -r $1»»dnl
)dnl
dnl
dnl For element in array
dnl
define(«fore»,
««for e in "${$1[@]}"; do
	$2
done»»dnl
)dnl
dnl
dnl For index in array
dnl
define(«fori»,
««for i in "${!$1[@]}"; do
	$2
done»»dnl
)dnl
dnl
dnl Sort array
dnl
define(«sorta»,
««$1=($(»echo(«"${$1[@]}"»)«|sort))»»dnl
)dnl
