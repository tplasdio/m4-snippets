dnl Static m4 snippets for Perl
dnl
dnl Shebang
dnl
define(`shebang',
``#!/usr/bin/perl'')dnl
dnl
dnl Entry point
dnl
define(`entrypoint',
``unless(caller) {
	&main();
}''dnl
)dnl
dnl
dnl Function
dnl
define(`fun',
``sub $1 {
	my ($2) = @_;
	$3
}''dnl
)dnl
dnl
dnl Function without collected parameters
dnl
define(`fun0',
``sub $1 {
	$2
}''dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
`shebang()`
use strict;
use warnings;
# use feature "say";

'entrypoint()

fun0(main,$1)'dnl
)dnl
dnl
dnl Print to stderr
dnl
define(`stderr',
``print STDOUT $*, "\n";''dnl
)dnl
dnl
dnl For in iterable
dnl
define(`for',
``for my $e ('`@$1) {
	
}''dnl
)dnl
dnl
dnl Argument parsing
dnl
define(`getopt',
``use Getopt::Long qw('`GetOptions);
Getopt::Long::Configure qw('`gnu_getopt);

GetOptions('`
	"a|alfa" => \$Opt_a,
	"b|beta" => \$Opt_b,
	"h|help" => \$Opt_h,
) or die ('`"Error reading arguments\n");
''dnl
)dnl
dnl
dnl Class
dnl
define(`clase',
``#!/usr/bin/perl
package $1;

use strict;
use warnings;

'fun0(`new',
`bless {
		_a1 => $_[1],
		_a2 => $_[2]
	}, $_[0];')`

1;''dnl
)dnl
