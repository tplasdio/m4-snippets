dnl Static m4 snippets for POSIX shell
dnl
include(`lib/indent.m4')dnl
dnl
changequote(«,»)dnl
dnl
define(«shebang»,
««#!/bin/sh»»)dnl
dnl
define(«entrypoint»,
««main "$»«@" || exit $?»»dnl
)dnl
dnl
dnl Function
dnl
define(«fun»,
««$1() {
	$2
}»»dnl
)dnl
dnl
dnl Initialization
dnl
define(«setup»,
«shebang()

fun(main, $1)

entrypoint()»dnl
)dnl
dnl
dnl Initialize gettext translations
dnl
define(«gettext»,
««TEXTDOMAIN="${0##*/}" TEXTDOMAIN="${TEXTDOMAIN%%.*}"
export TEXTDOMAIN TEXTDOMAINDIR="${0%/*}/locale"
. gettext.sh»»dnl
)dnl
dnl
dnl Argument parsing with getoptions
dnl
define(«getoptions»,
««[ $»«# -eq 0 ] && Opt_Noarg=1

parser() {
	setup REST help:usage -- ""
	msg -- '' 'USO:' "  ${»«0##*/} [OPCIONES] [ARGS]"
	msg -- '' 'OPCIONES:'
	flag   Opt_a -a --alfa -- ""
	flag   Opt_b -b --beta -- ""
	param  Opt_c -c -- ""
	option Opt_d -d -- ""
	disp   :usage -h --help -- "Mostrar esta ayuda"
	disp VERSION --version
}

eval "$(»«getoptions parser) exit 1"»»dnl
)dnl
dnl
dnl Exit program
dnl
define(«exit»,
««exit $1 »»dnl
)dnl
dnl
dnl Safe printing
dnl
define(«echo»,
««printf -- "%s\n" $1»»dnl
)dnl
dnl
dnl Read stdin line into a variable, optionally with text before
dnl
define(«stdin»,
«ifelse($2,«»,«»,echo($2)
)dnl
«read -r $1»»dnl
)dnl
dnl
dnl Print to stderr
dnl
define(«stderr»,
««>&2 printf -- "%s" $*»»dnl
)dnl
dnl
dnl Bucle rango
dnl
define(«forr»,
«define(«_p3», ifelse($3,«»,«1»,$3))dnl
«_i=$1; while [ $»«_i -le $2 ]; do
	
	_i=$»«((_i+»_p3()«))
done»»dnl
)dnl
dnl
dnl Bucle en
dnl
define(«fore»,
«for _ in "${$1[@]}"; do
	
done»dnl
)dnl
dnl
dnl Repeat character with dd
dnl
define(«ddfor»,
«dd if=/dev/zero bs=$1 count=1 2>/dev/null \
| tr '\0' "$2"»dnl
)dnl
dnl
dnl Loop with awk
dnl
define(«awkfor»,
«awk '{ print "'"«$1»"' " $«»0 | "sh" } END { close("sh") }'»dnl
)dnl
dnl
dnl Loop with dd+awk
dnl
define(«ffor»,
«ddfor(»«$1, \n) \
| awkfor($2)»dnl
)dnl
dnl
dnl Column tab
dnl
define(«columnt»,
««column -t -s "	"»»dnl
)dnl
