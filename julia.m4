dnl Static m4 snippets for Julia
dnl
dnl Shebang
dnl
define(`shebang',
``#!/usr/bin/julia'')dnl
dnl
dnl Shebang for Julia client of DaemonMod
dnl
define(`dshebang',
``#!/usr/bin/env juliad'')dnl
dnl
dnl Entry point
dnl
define(`entrypoint',
``if abspath(PROGRAM_FILE) == @__FILE__
	main($1)
end''dnl
)dnl
dnl
dnl Function
dnl
define(`fun',
``function $1($2)
	$3
end''dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
`shebang()

fun(`main',,$1)

entrypoint()'dnl
)dnl
dnl
dnl Initialization for DaemonMod
dnl
define(`dsetup',
`dshebang()

fun(`main',,$1)

main()'dnl
)dnl
dnl
dnl Exit program
dnl
define(`exit',
``exit($1)''dnl
)dnl
