dnl Static m4 snippets for Raku
dnl
define(`shebang',
``#!/usr/bin/rakudo'')dnl
dnl
dnl Function
dnl
define(`fun',
``sub $1($2){
	$3
}''dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
`shebang()`

#BEGIN{}

my %*SUB-MAIN-OPTS =
	:named-anywhere,
	:bundling
;

'fun(MAIN,$2,$1)
dnl parameters as 2° argument just for consistency with other language snippets

#END{}'dnl
)dnl
dnl
dnl Print to stderr
dnl
define(`printerr',
``$'`*ERR.say($*);''dnl
)dnl
dnl
dnl Class
dnl
define(`clase',
``class $1 {
	has $.a1;
	has $!a2;
	has $.a3 is rw;
	method metodo1 {
		;
	}
}''dnl
)dnl
