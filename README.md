# M4-Snippets

Static snippets for different programming languages, written as `m4` macros.

## Dependencies
- `m4`
- `sed` (for snippets with indentation macros)

## Usage

Start writing a file, let's say `hello.py`:

```python
setup(print("hello world"))
```

Then run the command:

```sh
m4 python.m4 hello.py
```

And the snippet in `hello.py` would have expanded to:

```python
#!/usr/bin/python3

# import sys
# import os

def main():
    print("hello world")

if __name__ == "__main__":
    main()
```

You are very encorauged to read the source and make your own snippets or modify existing ones. They are easy to understand and you can read the full [manual of m4](https://www.gnu.org/software/m4/manual/).
Feel free to submit merge requests.

## Why?

`m4` maybe old, but is very powerful, extensible and universal, available in almost any Unix system. It is also easy to get started, although hard to master. Many snippet engines for text editors or IDEs have difficult syntax and are mostly specific to that particular application, meaning you probably couldn't port your snippets to another editor, but with `m4` you will always be able to run them from the command line, to any file type, and integrate them with your editor in a few lines of code, see [below](#integration-in-vim) for an example.

Although static snippets are not as interactive and well-integrated with the editor as dynamic ones, the power comes from being able to run arbitrary `m4` code like recursive macros, do arithmetic, conditionals, include libraries, and invoke shell commands (which also means you can extend your snippets with your favorite programming language(s)!).

* Maybe in the future I'll find a way to make them dynamic with a plugin for an editor.

## Integration in Vim

If you are using GNU `m4`, put these files in a directory set in the `$M4PATH` environment variable.

Then from Vim you can for example write `setup`, return to normal mode and run:

```vim
:.!m4 python.m4 -
```

to expand snippets in the current line.

You can also map key combinations in your configuration file (`.vimrc` for Vim, or `init.vim` for Neovim):

```vim
"Expand all macros in file
nnoremap <leader>rm :w<CR>:exec "%!m4 " . &filetype .  ".m4 " . "%:p"<CR>
"Expand macros in selected lines
xnoremap <leader>m  :<C-u>exec "\'<,\'>!m4 " . &filetype . ".m4 -"<CR>
"Expand macros in current line
nnoremap <leader>mm :exec ".!m4 " . &filetype . ".m4 -"<CR>
```

Or in `init.lua` for Neovim:

```lua
vim.api.nvim_set_keymap('n','<leader>rm',':w<CR>:exec "%!m4 " . &filetype .  ".m4 " . "%:p"<CR>', { noremap = true })
vim.api.nvim_set_keymap('x','<leader>m' ,':<C-u>exec "\'<,\'>!m4 " . &filetype . ".m4 -"<CR>', { noremap = true })
vim.api.nvim_set_keymap('n','<leader>mm',':exec ".!m4 " . &filetype . ".m4 -"<CR>',{ noremap = true})
```

If your `m4` doesn't read `$M4PATH` nor does it have the `-I` flag to include files from a certain directory, you would have to include the snippet file in the same directory as your code, or alternatively I have a shell script `m4s` that reads them from the `$SNIPPETS_DIR` environment variable or `~/.local/share/m4s/`.

* Maybe in the future I'll make a plugin

## License
GPLv3
