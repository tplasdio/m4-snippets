dnl Static m4 snippets for C
dnl
dnl Function
dnl
define(`fun',
`define(`_p4', ifelse($4,`',void,$4))dnl
_p4()` $1($2) {
	$3
}''dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
``#include <stdlib.h>
#include <stdio.h>

'fun(`main', `int argc, char** argv',`$1
	return 0;', `int')`''dnl
)dnl
dnl
dnl Exit program
dnl
define(`exit',
``exit($1);''dnl
)dnl
dnl
dnl Read stdin line into a variable
dnl
define(`stdin',
``scanf("%[^\n]s", $1);''dnl
)dnl
dnl
dnl Print to stderr
dnl
define(`stderr',
``fprintf(stderr, $*);''dnl
)dnl
dnl
dnl Array length
dnl
define(`len',
``sizeof('`$1)/sizeof('`$1[0])''dnl
)dnl
dnl
dnl For
dnl
define(`for',
``int i;
for(i=0; i < 'len($1)`; i++){
	$2;
}''dnl
)dnl
dnl
dnl For range
dnl
define(`forr',
`define(`_p3', ifelse($3,`',++,+=$3))dnl
`int i;
for(i=$1; i<=$2; i'_p3()`){
	$4;
}''dnl
)dnl
dnl
dnl "Function" with default parameters
dnl
define(`fundefault',
``typedef struct{tipo p1; tipo p2;} $1_args;

tipo $1_base('`tipo p1, tipo p2){
    comandos;
}

tipo $1_wrapper('`$1_args args){
    tipo p1_default = args.p1 ? args.p1 : ;
    tipo p2_default = args.p2 ? args.p2 : ;
    return $1_base('`p1_default, p2_default);
}

#define $1('`...) $1_wrapper(('`$1_args){__VA_ARGS__});''dnl
)dnl
dnl
dnl Hash table of file type and POSIX function, similar to shell 'test' syntax
dnl
define(`type',
`ifelse(
$1, `f', `S_ISREG', dnl regular file
$1, `d', `S_ISDIR', dnl directory
$1, `l', `S_ISLNK', dnl softlink
$1, `p', `S_ISFIFO', dnl named pipe
$1, `S', `S_ISSOCK', dnl socket
)`'')dnl
dnl
dnl
dnl Check file type
dnl
define(`test',
``#include <unistd.h>

struct stat st;
stat('`$1, &st);
if ('type($2)`('`st.st_mode)) {
	;
}
''dnl
)dnl
