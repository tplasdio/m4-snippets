dnl Static m4 snippets for TeX and LaTeX
dnl
define(`setup',
``\documentclass{article}
\author{'syscmd(
`printf -- "%s" "$(whoami)"'
)`}
\title{'syscmd(
file="__file__"
printf -- "%s" "$(basename "${file%%.*}")"
)`}
\date{'syscmd(
printf -- "%s" "$(date "+%d de %B de %Y")"
)`}

\begin{document}



\end{document}''dnl
)dnl
define(`tabla',
``\begin{table}
	%\label{mytable}
	%\caption{My table}
	%\centering

	\begin{tabular}{|c|c|c|}
		\hline
		cell11 & cell12 & cell13\\
		\hline
		cell21 & cell22 & cell23\\
		\hline
		cell31 & cell32 & cell33\\
		\hline
	\end{tabular}
\end{table}''dnl
)dnl
