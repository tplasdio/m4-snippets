dnl Static m4 snippets for Awk
dnl
dnl Shebang
dnl
define(`shebang',
``#!/usr/bin/awk -f'')dnl
dnl
dnl Initialization
dnl
define(`setup',
`shebang()`

function main() {
	
}

BEGIN {
	main()
}''dnl
)dnl
