dnl Static m4 snippets for SQL
dnl
changequote(«, »)dnl
dnl
define(«shebang»,
«#!/usr/bin/mariadb»)dnl
dnl
define(«pk»,
«INT PRIMARY KEY NOT NULL AUTO_INCREMENT»dnl
)dnl
dnl
dnl Load CSV
dnl
define(«loadcsv»,
«LOAD DATA LOCAL INFILE $1
INTO TABLE csvtable
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS»»dnl
)dnl
