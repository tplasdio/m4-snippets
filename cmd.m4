dnl Static m4 snippets for cmd.exe
dnl
dnl Shebang
dnl
define(`shebang',
``#!/usr/bin/env winescript'')dnl
dnl
dnl Entry point
dnl
define(`entrypoint',
``call :main
exit''dnl
)dnl
dnl
dnl Function
dnl
define(`fun',
``:$1
setlocal
	$2
endlocal
exit /b 0''dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
`shebang()`
@echo off

'entrypoint()

fun(main, $1)'dnl
)dnl
