dnl Static m4 snippets for Rust
dnl
dnl Initialization
dnl
define(`setup',
``// use std::env;
// use std::collections::HashMap;

fn main() {

}''dnl
)dnl
dnl
dnl Print with final line break
dnl
define(`echo',
``println!($*);''dnl
)dnl
dnl
dnl Exit program
dnl
define(`exit',
``std::process::exit($1);''dnl
)dnl
dnl
dnl For array
dnl
define(`fora',
``for i in $1.iter() {
    ;
}''dnl
)dnl
dnl
dnl For enumerated
dnl
define(`fore',
``for (i,&e) in $1.iter().enumerate() {
    ;
}''dnl
)dnl
dnl
dnl For key-value
dnl
define(`forkv',
``for (k,v) in &$1 {
    ;
}''dnl
)dnl
dnl
dnl For vector
dnl
define(`forv',
``for i in &$1 {
    ;
}''dnl
)dnl
dnl
dnl For string
dnl
define(`fors',
``for i in $1.chars() {
    ;
}''dnl
)dnl
dnl
dnl For range
dnl
define(`forr',
``for _ in $1..=$2 {
    ;
}''dnl
)dnl
dnl
dnl Error handling
dnl
define(`error',
``let $1 = match $1 {
  Ok('`x) => x,
  Err('`e) => ,
};''dnl
)dnl
dnl
dnl Null handling
dnl
define(`nulo',
``if let Some('`$1) = $1 {
    ;
} else {
    ;
}''dnl
)dnl
