dnl Static m4 snippets for Python
dnl
include(`lib/indent.m4')dnl
dnl
dnl Shebang
dnl
define(`shebang',
``#!/usr/bin/python3'')dnl
dnl
dnl Entry point
dnl
define(`entrypoint',
``if __name__ == "__main__":
    main($1)''dnl
)dnl
dnl
dnl Entry point with systemexit
dnl
define(`entrypoint2',
``if __name__ == "__main__":
    raise SystemExit(main($1))''dnl
)dnl
dnl
dnl Jupyter entrypoint
dnl
define(`jupyter_entrypoint',
``if os.path.split('`sys.argv[0])[-1] == "ipykernel_launcher.py":
    main()
el'entrypoint(sys.argv[1:])'dnl
)dnl
dnl
dnl Function
dnl
define(`fun',
``def $1($2)$4:
    $3''dnl
)dnl
dnl
dnl Coroutine function
dnl
define(`afun',
``async 'fun($*)'dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
`shebang()`

# import sys
# import os

'fun(`main',,`$1
    return 0',` -> int')

entrypoint2()'dnl
)dnl
dnl
dnl Async initialization
dnl
define(`asetup',
`shebang()`

# import sys
# import os
import asyncio

'afun(amain,,$1)

fun(main,, `asyncio.run(amain())')

`'entrypoint()`''dnl
)dnl
dnl
dnl Jupyter async initialization
dnl
define(`jasetup',
`shebang()`

# %%
import os,sys
import asyncio

# %%
'afun(amain,,$1)

`# %%'
fun(main,, `if IN_JUPYTER:
        jupyter_loop = asyncio.get_event_loop()
        asyncio.run_coroutine_threadsafe(amain(), jupyter_loop)
    else:
        asyncio.run(amain())')`

# %%
if os.path.split(sys.argv[0])[-1] == "ipykernel_launcher.py":
    IN_JUPYTER = True
    main()
elif __name__ == "__main__":
    IN_JUPYTER = False
    main()''dnl
)dnl
dnl
dnl Jupyter cell
dnl
define(`jc', ``# %%
$1'')dnl
dnl
dnl Jupyter text
dnl
define(`jj',
``# %%
"""
 $1
"""
'')dnl
dnl
dnl Exit program
dnl
define(`exit',
``exit($1)''dnl
)dnl
dnl
dnl Read stdin line into a variable, with text before
dnl
define(`stdin',
``$1 = input($2)''dnl
)dnl
dnl
dnl Print to stderr
dnl
define(`stderr',
``print('`$*, file=sys.stderr)''dnl
)dnl
dnl
dnl For in iterable
dnl
define(`for',
``for _ in $1:
    $2''dnl
)dnl
dnl
dnl For range
dnl
define(`forr',
``for _ in range($1,$2,$3):
    $4''dnl
)dnl
dnl
dnl For enumerated
dnl
define(`fore',
``for i,e in enumerate($1):
    ''dnl
)dnl
dnl
dnl For key-value
dnl
define(`forkv',
``for k,v in $1.items():
    ''dnl
)dnl
dnl
dnl Clase
dnl
define(`clase',
``class $1:
    def __init__('`self,p1,p2):
        self.a1 = p1
        self.a2 = p2''dnl
)dnl
dnl
dnl Clase con sus propios atributos y métodos
dnl
define(`clasef',
``class $1:
    # Class attributes
    a1 = *
    a2 = *
    # Instance attributes
    def __init__('`self,p1,p2):
        self.a3 = p1
        self.a4 = p2
    # Class method
    @classmethod
    def metodo1('`cls):
        pass
    # Instance method
    def metodo2('`self):
        pass''dnl
)dnl
dnl
dnl Clase decorador
dnl
define(`cdecor',
``class $1:
    def __init__('`self,function):
        self.function = function
    def __call__('`self):
        
        # self.function()''dnl
)dnl
dnl
dnl Clase decorador con parámetros
dnl
define(`cddecor',
``class $1:
    def __init__('`self, p1, p2):
        self.p1 = p1
        self.p2 = p2
    def __call__('`self, function):
        def _dec():
            
            # function()
            
        return _dec''dnl
)dnl
dnl
dnl Decorador
dnl
define(`decor',
``def $1('`function):
    def _decorator():
        
        #function()
        
    return _decorator''dnl
)dnl
dnl
dnl Decorador con parámetros
dnl
define(`ddecor',
``def $1('`p1,p2):
    '_indent_not1(`',decor(__decorator))`
    return __decorator''dnl
)dnl
dnl
dnl Clase gestor de contexto
dnl
define(`gestor',
``class $1:
    def __init__('`self,p1):
        self.a1 = p1
    def __enter__('`self):
        return x
    def __exit__('`self, type, value, traceback):
        ''dnl
)dnl
dnl
dnl Decorador gestor de contexto
dnl
define(`gestor2',
``from contextlib import contextmanager
@contextmanager
def $1('`p1):
    try:
        yield x
    finally:
        ''dnl
)dnl
dnl
dnl Argument parsing
dnl
define(`argparse',
``import argparse

def parse_cmdargs('`cmdargs: list = []):
    """Command-line argument parsing"""
    parser = argparse.ArgumentParser('`description = """ """)

    parser.add_argument('`"-a", "--alpha",
            help = "",
            action = "store_true"
            )

    parser.add_argument('`"-b", "--beta",
            help = "",
            )

    return parser.parse_args('`cmdargs)

args = parse_cmdargs('`sys.argv[1:])''dnl
)dnl
define(`randomforest',
``import pandas as pd
from sklearn.ensemble import RandomForestRegressor
#from sklearn.model_selection import train_test_split
#from sklearn.metrics import mean_absolute_error

# train_X, val_X, train_y, val_y = train_test_split('`X, y, random_state=1)

modelo = RandomForestRegressor('`random_state=1)
modelo.fit('`train_X, train_y)

predicted_X = modelo.predict('`val_X)
#mae = mean_absolute_error('`predicted_X, val_y)''dnl
)dnl
