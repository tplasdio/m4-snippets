dnl Static snippets for systemd configuration unit files
dnl
define(`servicio',
``[Unit]
Description=tu descripción
[Service]
Type=simple
ExecStart=[fichero] [args]  # Comando a ejecutar
User=usuario
# ↓ Opcional para habilitar
#[Install]
#WantedBy=multi-user.target
#WantedBy=graphical.target''dnl
)dnl
define(`temporizador',
``[Unit]
Description=tu descripción
[Timer]
Unit=[servicio].service  # Homónimo por omisión
#OnBootSec=#min
#OnUnitActiveSec=#min
OnCalendar=díasemana año-mes-día hora:minuto:segundo
#Persistent=true
#[Install]
#WantedBy=timers.target
#WantedBy=multi-user.target
#WantedBy=graphical.target''dnl
)dnl
