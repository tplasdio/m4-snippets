dnl Static m4 snippets for regex
dnl
include(`lib/loops.m4')dnl
dnl
dnl And, with positive lookaheads
dnl
define(`and',
	`m4_foreach(`i', ($@), `(?=.*i)')')dnl
dnl
dnl Not, with negative lookahead
dnl
define(`not',
	``(?:(?!$1).)*'')dnl
dnl
dnl Or
dnl    TODO: argument to decide if there's a final |
dnl
define(`or',
	``($1'm4_foreach(`i', (shift($@)), `|i')'`)')dnl
dnl
dnl In-between
dnl
define(`in',
	``(?<=$1)(.*)(?=$2)'')dnl
dnl
dnl From-to
dnl
define(`from',
	``$1'm4_foreach(`i', (shift($@)), `.*i')')dnl
dnl
dnl Exact word (separated by whitespace)
dnl
define(`exact',
	``\b$1\b'')dnl
dnl
dnl Hexadecimal
dnl
define(`hex',
	``0x[0-9a-fA-F]{7}'')dnl
dnl
dnl Variable names for m4, shell
dnl
define(`variable',
	``^[a-zA-Z_][a-zA-Z0-9_]*$'')dnl
dnl
dnl Email W3C
dnl
changequote(«,»)dnl
define(«email»,
	««^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$»»)dnl
dnl
dnl Email (RFC 5322)
dnl
define(«emailf»,
	««(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])»»)dnl
