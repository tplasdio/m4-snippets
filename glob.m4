dnl Static m4 snippets for globbings
dnl
dnl Classic POSIX classes
dnl
define(`_lower_' , ``[a-z]'')dnl
define(`_upper_' , ``[A-Z]'')dnl
define(`_alpha_' , ``[A-Za-z]'')dnl
define(`_digit_' , ``[0-9]'')dnl
define(`_alnum_' , ``[A-Za-z0-9]'')dnl
define(`_word_'  , ``[A-Za-z0-9_]'')dnl
define(`_blank_' , ``[ \t]'')dnl
define(`_space_' , ``[ \t\n\r\f\v]'')dnl
define(`_ascii_' , ``[\x00-\x7F]'')dnl
define(`_xdigit_', ``[0-9A-Fa-f]'')dnl
define(`_cntrl_' , ``[\x00-\x1F\x7F]'')dnl
define(`_graph_' , ``[^ '_cntrl_()]`'')dnl
define(`_print_' , ``['_graph_()` ]'')dnl
changequote(«,»)dnl
define(«_punct_» , ««[-!"#$%&'()*+,./:;<=>?@[]^_`{|}~]»»)dnl

