dnl Static m4 snippets for PHP
dnl
dnl Shebang
dnl
define(`shebang',
``#!/usr/bin/php'')dnl
dnl
dnl Entry point
dnl
define(`entrypoint',
``if ('`!count('`debug_backtrace('`DEBUG_BACKTRACE_IGNORE_ARGS))) {
	main();
}''dnl
)dnl
dnl
dnl Function
dnl
define(`fun',
``function $1($2) {
	$3
}''dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
`shebang()`
<?php
'fun(main,$1)

entrypoint()
?>'dnl
)dnl
