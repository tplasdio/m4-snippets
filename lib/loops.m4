dnl  Loop macros
dnl    Taken from the GNU m4 repo, GPLv3
dnl
define(`_arg1', `$1')dnl
dnl
dnl  For loop
dnl    Example: m4_forloop(`i', `1', `8', `text i ')
dnl
define(`m4_forloop', `pushdef(`$1', `$2')_m4_forloop($@)popdef(`$1')')dnl
define(`_m4_forloop',
       `$4`'ifelse($1, `$3', `', `define(`$1', incr($1))$0($@)')')dnl
dnl
dnl  For each
dnl    Example: m4_foreach(`i', (one, two, three), `text i ')
dnl                            ↑ Cannot be macro names
dnl
define(`m4_foreach', `pushdef(`$1')_m4_foreach($@)popdef(`$1')')dnl
define(`_m4_foreach', `ifelse(`$2', `()', `',
  `define(`$1', _arg1$2)$3`'$0(`$1', (shift$2), `$3')')')dnl
dnl
dnl  For each quoted
dnl    Example: m4_foreachq(`i', ```a'', ``(b'', ``c)''', `text i ')
dnl
dnl define(`m4_foreachq', `pushdef(`$1')_m4_foreachq($@)popdef(`$1')')dnl
dnl define(`_m4_foreachq', `ifelse(quote($2), `', `',
dnl   `define(`$1', `_arg1($2)')$3`'$0(`$1', `shift($2)', `$3')')')dnl
