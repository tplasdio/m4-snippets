dnl Indentation macros
dnl
dnl include(./meta.m4)
dnl
define(`m4_echo',`$*')dnl
dnl
dnl  Identation character (4 spaces by default), tab if 't' is 1° arg
dnl     I still can't make _indent_char be replaced correctly inside sed
dnl
define(`_indent_char',
`ifelse(
$1, `', `    ',
$1, `t', `	')`'')dnl
dnl
dnl  Indent all lines
dnl
define(`_indent',
`syscmd(sed -E "s|(.*)|    \1|" <<\EOF
$*
EOF)'dnl
)dnl
dnl
dnl  Indent all lines in 2° arg except the first line
dnl    If 1° arg is 't', indent with tab, if it's '' with 4 spaces
dnl
define(`_indent_not1',
`patsubst(shift($@),`
', `
_indent_char($1)')'dnl
)dnl
dnl
dnl  Indent a range of lines, as in sed format
dnl
define(`_indent_range',
`syscmd(sed -E ```'$1,$2s|(.*)|    \1|'' <<\EOF
m4_echo(shift(shift($@)))
EOF)'dnl
)dnl
dnl TODO:
dnl - un condicional que detecta si $1 es una macro ya definida o no
dnl - expandir todas las macros pasadas como argumentos
define(`_indent_macro',
`syscmd(sed -E "s|(.*)|    \1|" <<\EOF
$1()
EOF)'dnl
)dnl
