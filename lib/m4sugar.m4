dnl  Repeat string
dnl    Example: m4_repeat(n_times, text)
dnl      Taken from not-autotools, (C) madmurphy, GPLv3
dnl
define(`m4_repeat',
	`ifelse(eval(`$1 > 0'), `1',
		`$2`'m4_repeat(decr(`$1'), `$2')')')dnl
