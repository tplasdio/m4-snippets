dnl Common aliases
dnl
changequote(«,»)dnl
dnl
define(«alias_asciiupper»,
««ABCDEFGHIJKLMNOPQRSTUVWXYZ»»dnl
)dnl
dnl
define(«alias_asciilower»,
««abcdefghijklmnopqrstuvwxyz»»dnl
)dnl
dnl
define(«alias_numbers»,
««0123456789»»dnl
)dnl
dnl
define(«alias_asciialnum»,
«alias_numbers()alias_asciilower()alias_asciiupper()»dnl
)dnl
dnl
define(«alias_asciiwhitespace»,
««»»dnl
)dnl
dnl
define(«alias_controlchars»,
««	
»»dnl
)dnl
dnl
define(«alias_shellsafechars»,
«alias_asciialnum()«%+,./:=@_^!-»»dnl
)dnl
dnl
define(«alias_asciichars»,
«alias_controlchars()«"#$&'()*;<>?[\]`{|}~»alias_shellsafechars()»dnl
)dnl
