dnl Static m4 snippets for C++
dnl
include(`lib/loops.m4')dnl
dnl
dnl Function
dnl
define(`fun',
`define(`_p4', ifelse($4,`',`void',$4))dnl
_p4()` $1($2) {
	$3
}''dnl
)dnl
dnl
dnl Function with auto -> return type
dnl
define(`fun2',
`define(`_p4', ifelse($4,`',`-> void',$4))dnl
`auto $1($2) '_p4()` {
	$3
}''dnl
)dnl
dnl
dnl Template
dnl
define(`tmpl',
``template<typename $1'm4_foreach(`i', (shift($@)), `, typename i')>')dnl
dnl
dnl Template function
dnl
define(`tfun',
``template<typename $1>
'fun(shift($@))`''dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
``#include <iostream>
//using std::vector, std::array;

'fun2(`main', `int argc, char** argv',`$1
	return 0;',`-> int')`''dnl
)dnl
dnl
dnl Array length
dnl
define(`len',
``sizeof('`$1)/sizeof('`$1[0])''dnl
)dnl
dnl
dnl Exit program
dnl
define(`exit',
``exit($1);''dnl
)dnl
dnl
dnl Print with final line break
dnl
define(`echo',
``std::cout << $1 << "\n";''dnl
)dnl
dnl
dnl Type of a variable
dnl
define(`typeof',
``typeid($1).name()''dnl
)dnl
dnl
dnl Read stdin line into a variable
dnl
define(`stdin',
``std::cin >> $1;''dnl
)dnl
dnl
dnl Print to stderr
dnl
define(`stderr',
``std::cerr << $*;''dnl
)dnl
dnl
dnl For array
dnl
define(`for',
``int i;
for(i=0; i < 'len($1)`; i++){
	$2;
}''dnl
)dnl
dnl
dnl For range
dnl
define(`forr',
`define(`_p3', ifelse($3,`',++,+=$3))dnl
`int i;
for(i=$1; i<=$2; i'_p3()`){
	$4;
}''dnl
)dnl
dnl
dnl For vector
dnl
define(`forv',
``for(tipo e: $1){
	$2;
}''dnl
)dnl
dnl
dnl For vector, pointer iterator
dnl
define(`forvp',
``for(auto i=$1.begin(); i!=$1.end();i++) {
	$2;
}''dnl
)dnl
dnl
dnl Variadic function
dnl
define(`variadica',
``#include <stdarg.h>
tipo $1('`tipo...);

tipo $1('`int argsc...){
    va_list args;
    va_start('`args, argsc);
    for ('`int i=0; i < argsc; i++) {
        tipo arg = va_arg('`args, tipo);
        comandos;
    }
}''dnl
)dnl
dnl va_end('`args);
dnl
dnl Class
dnl
define(`clase',
``class $1 {
	private:
		tipo _a1;
		tipo _a2;
	public:
		$1() {
			a1 = ;
			a2 = ;
		}

		$1('`tipo a1, tipo a2) {
			a1 = ;
			a2 = ;
		}
};''dnl
)dnl
