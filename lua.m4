dnl Static m4 snippets for Lua
dnl
dnl Shebang
dnl
define(`shebang',
``#!/usr/bin/luajit'')dnl
dnl
dnl Entry point
dnl
define(`entrypoint',
``if not pcall('`debug.getlocal, 4, 1) then
	main()
end''dnl
)dnl
dnl
dnl Function
dnl
define(`fun',
``function $1($2)
	$3
end''dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
`shebang()

fun(main,,$1)

entrypoint()'dnl
)dnl
dnl
dnl Exit program
dnl
define(`exit',
``os.exit($1)''dnl
)dnl
dnl
dnl Printf like-C
dnl
define(`printf',
``io.write(string.format($*))''dnl
)dnl
dnl
dnl Read stdin line into a variable, with text before
dnl
define(`stdin',
``$1 = io.read($2)''dnl
)dnl
dnl
dnl Print to stderr
dnl
define(`stderr',
``io.stderr:write('`$*.."\n")''dnl
)dnl
dnl
dnl For
dnl
define(`for',
``for _ in $1 do
	
end''dnl
)dnl
dnl
dnl For range
dnl
define(`forr',
`define(`_range', `ifelse(eval($#==1), `1', `1,$1',`$*')')dnl
`for _ = '_range()` do
	
end''dnl
)dnl
dnl
dnl For enumerated
dnl
define(`fore',
``for i,e in ipairs($1) do
	
end''dnl
)dnl
dnl
dnl For key-value
dnl
define(`forkv',
``for k,v in pairs($1):
	
end''dnl
)dnl
dnl
dnl Class
dnl
define(`clase',
``$1 = {}

function $1:new('`t)
	t = t or {}
	setmetatable('`t,self)
	self.__index = self

	self.a1 = t.p1
	self.a2 = t.p2
	return t
end''dnl
)dnl
dnl
dnl Class with class attributes and methods
dnl
define(`clasef',
``$1 = {
	# Class attributes
	a1 = *,
	a2 = *,

	# Class method
	metodo2 = function()
		
	end
}

function $1:new('`t)
	t = t or {}
	setmetatable('`t,self)
	self.__index = self

	# Instance attributes
	self.a3 = t.p1
	self.a4 = t.p2

	# Instance method
	function self:metodo1()
		
	end

	return t
end''dnl
)dnl
dnl
dnl Argument parsing
dnl
define(`argparse',
``local argparse = require "argparse"

function parse_cmdargs('`cmdargs)
	local parser = argparse()
		:name ""
		:description ""

	parser:flag('`"-a --alfa")
		:description('`"")

	parser:option('`"-b --beta")
		:description('`"")

	return parser:parse('`cmdargs)
end

local args = parse_cmdargs('`arg)''dnl
)dnl
