dnl Static m4 snippets for JavaScript
dnl
dnl Shebangs
dnl
define(`shebang',
``#!/usr/bin/js78'')dnl
define(`shebang_node',
``#!/usr/bin/node'')dnl
dnl
dnl Entry point (only NodeJS, ¿CoffeeScript?)
dnl
define(`entrypoint_node',
``if (require.main === module) {
	main();
}''dnl
)dnl
dnl
dnl Function
dnl
define(`fun',
``function $1($2) {
	$3;
}''dnl
)dnl
dnl
dnl Anonymous arrow function
dnl
define(`lambda2',
``($1) => {
	$2;
}''dnl
)dnl
dnl
dnl Function expression
dnl
define(`fune',
``const $1 = function($2) {
	$3;
};''dnl
)dnl
dnl
dnl Arrow function
dnl
define(`funa',
``const $1 = 'lambda2($2,$3);'dnl
)dnl
dnl
dnl Async arrow function
dnl
define(`afuna',
``const $1 = async 'lambda2($2,$3);'dnl
)dnl
dnl
dnl Initialization
dnl
define(`setup',
`shebang()

fun(`main',,`$1')

`main();''dnl
)dnl
dnl
dnl Initialization for Node
dnl
define(`nsetup',
`shebang_node()

funa(`main',,`$1')

entrypoint_node()'dnl
)dnl
dnl
dnl Async setup for Node
dnl
define(`ansetup',
`shebang_node()

afuna(`amain',,`$1')

funa(`main',,`amain()')

entrypoint_node()'dnl
)dnl
dnl
dnl Print to console
dnl
define(`echo',
``console.log($*);''dnl
)dnl
dnl
dnl For range
dnl
define(`forr',
`define(`_p3', ifelse($3,`',++,+=$3))dnl
`for(let i=$1; i<=$2; i'_p3()`){
	$4;
}''dnl
)dnl
dnl
dnl For in iterable
dnl
define(`for',
``for (const _ of $1) {
	$2
}''dnl
)dnl
dnl
dnl For in object
dnl
define(`fori',
``for (const _ in $1) {
	$2
}''dnl
)dnl
dnl
dnl Class
dnl
define(`clase',
``class $1 {
	constructor(p1,p2){
		this.a1 = p1;
		this.a2 = p2;
	}
	method() {
		;
	}
};''dnl
)dnl
dnl
dnl Class expression
dnl
define(`clasee',
``const $1 = class {
	constructor(p1,p2){
		this.a1 = p1;
		this.a2 = p2;
	}
	method() {
		;
	}
};''dnl
)dnl
dnl
dnl Foreach in array
dnl
define(`fora',
``$1.forEach('fun(,`e,i,$1',$2)`)''dnl
)dnl
dnl
dnl Foreach in array, arrow function
dnl
define(`fora2',
``$1.forEach('lambda2(`e,i,$1',$2)`)''dnl
)dnl
dnl
dnl Map in array, arrow function
dnl
define(`map2',
``$1.map('lambda2(`e,i,$1',$2)`)''dnl
)dnl
